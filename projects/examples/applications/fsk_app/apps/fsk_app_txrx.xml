<?ls /devxml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<!--############################################################################

  “fsk_app_txrx.xml”  (TX and RX)

  The intent of the OpenCPI FSK suite of example applications is to facilitate a
  new user’s understanding of the OpenCPI framework structure, show ease of
  usage, and convey value of portability. An OpenCPI application typically
  defines a specified signal processing functionality implemented using OpenCPI
  project components. This application specifically transmits a data file (e.g.,
  idata/opencpi.jpg) using a standard frequency shift keying (FSK) modulation
  scheme between the transmit and receiver ports of an OpenCPI SDR platform.
  Applications represent end-to-end data processing flow from data source to
  data sink. Data flow in this example begins and ends with the ‘file_read.rcc’
  and ‘file_write.rcc’ workers. This application instantiates all required
  processing component workers, defines their required property and parameter
  values, and defines the sequenced connectivity between each component worker.

  When a user executes an application, the workers must be made available to
  OpenCPI runtime execution. Prior to runtime, a user must build these workers
  as a compiled RCC library or HDL binary assembly.  When a user runs the
  application, OpenCPI seeks these workers by searching for their symbolic links
  in their project’s artifacts directory. OpenCPI only searches those
  directories defined by the OCPI_LIBRARY_PATH environment variable. Many new
  users fail to remember to ensure this path is set correctly.  To execute this
  example application, the following directory paths should be set for each
  desired platform:

         [E310]
         OCPI_LIBRARY_PATH=~/opencpi/projects/core/artifacts/:
                           ~/opencpi/projects/ocpi.comp.sdr/artifacts/:
                           ~/opencpi/projects/examples/artifacts/:
                           ~/opencpi/projects/osps/ocpi.osp.e3xx/artifacts/
         [ZED]
         OCPI_LIBRARY_PATH=~/opencpi/projects/core/artifacts/:
                           ~/opencpi/projects/ocpi.comp.sdr/artifacts/:
                           ~/opencpi/projects/examples/artifacts/:
                           ~/opencpi/projects/assets/artifacts/
         [PLUTOSDR_CSTS]
         OCPI_LIBRARY_PATH=~/opencpi/projects/core/artifacts/:
                           ~/opencpi/projects/ocpi.comp.sdr/artifacts/:
                           ~/opencpi/projects/examples/artifacts/:
                           ~/opencpi/projects/osps/ocpi.osp.plutosdr/artifacts/

  In most instances, applications may be developed using components that are
  agnostic to HDL or RCC worker implementation. However, some component workers
  may require unique properties as in the case of the ‘data_unpack_uc_b’,
  ‘fir_filter_filter_scaled_s’, and ‘moving_average_filter_b’ component workers
  used in this application.  Thus, there are two example FSK applications that
  support the RCC only and mostly HDL cases.

  The example applications attempt to provide intuitive comments within the
  application file to facilitate understanding and meet the objectives of these
  examples.

  All applications that intend to execute processing on an SDR platform must
  interface with the platform hardware. This is accomplished using an HDL
  assembly regardless of any specific application worker being defined as an
  RCC or HDL worker. The following assemblies must be built for the desired
  platform to support this application and its symbolic link found in the
  project’s artifacts directory:

       [MOSTLY HDL]
                ~/opencpi/projects/examples/hdl/assemblies/fsk_app/
                  fsk_app_txrx_hdl_assy/fsk_app_txrx_hdl.xml
       [MOSTLY RCC]
                ~/opencpi/projects/examples/hdl/assemblies/fsk_app/
                  fsk_app_txrx_rcc_assy/fsk_app_txrx_rcc.xml

  Running the application from application directory:

    ~/opencpi/projects/examples/applications/fsk_app$ ocpirun apps/fsk_app_txrx.xml

  Running the application using the ACI:

    ~/opencpi/projects/examples/applications/fsk_app$./target-ubuntu20_04/fsk_app

  Alternatively, the input and output files may be modified and defined using
  OpenCPI run application options found in Table 11 of the OpenCPI Application
  Development Guide or the OpenCPI man pages at:

    https://opencpi.gitlab.io/releases/develop/man/

  Running the application using the ACI:

    ~/opencpi/projects/examples/applications/fsk_app$./target-ubuntu20_04/fsk_app

  Available options for the ACI application to modify application propertied may
  be viewed by adding the -h option to the above CLI command.

  ###########################################################################-->
  <!-- The 'done' declaration specifies when application execution is complete
       based on the "done" state of a specified worker. In this application, the
       discontinuity_gate_b is named "gate". When this worker state becomes
       "done", the  application will terminate.-->

<application done="gate">
  <!--
  ***********************************
  Transmit Data Source
  ***********************************
  -->
  <!--  The component 'file_read' is a data source worker and defined to execute
        on a specified processing platform.  In this application, the default
        platform is Ubuntu 20.04. By specifying a host processing platform, the
        component will implement an RCC worker. The property 'filename' defines
        the file on the local platform which to generate payload data to be
        transmitted.

        When the 'connect' option is defined with a component instance, it
        specifies the next component in the processing path that the output data
        sequence will be sent.  In this case, data will flow from the file_read
        component 'output' port to the 'input' port of the component named
        "packet".

        Note: For two component interfaces to connect, the connecting ports must
        be implemented using the same interface protocol
  -->
  <instance component="ocpi.core.file_read" connect="packet"
            container="rcc0">
    <property name="filename" value="idata/opencpi.jpg"/>
    <property name="messagesize" value="1024"/>
    <property name="messagesinfile" value="false"/>
    <property name="suppresseof" value="false"/>
    <!-- suppresseof property must be set to false to generate end-of-file (EOF)
    -->
  </instance>
  <!--
  ***********************************
  Transmit FSK path
  ***********************************
  -->
  <!-- The 'build_packet_uc' component receives the raw payload data bytes from
       the 'file_read' component.  Both the component's input and output ports
       support 'unsigned characters' as shown by the component name's suffix
       "_uc". The previous 'file_read' component currently does not conform to
       this naming convention since it is a legacy OpenCPI component.

       This  component applies header and tail bytes to the payload that
       indicate to the receiver the start and end of the payload.-->
  <instance component="ocpi.examples.build_packet_uc" name="packet"
            connect="unpack_bits"/>
  <!-- The 'data_unpack_uc_b' component breaks out the received payload bytes
       into individual data bits. The component suffix "_uc_b" indicates that
       the component's input port supports 'unsigned character' data and the
       output port supports 'binary' data.-->
  <instance component="ocpi.comp.sdr.communication.data_unpack_uc_b"
             name="unpack_bits" model="rcc" connect="sym_map">
  <!-- The 'name' option may be used to provide an abbreviated reference to a
       component instance.  The name may be used throughout the application to
       reference the specified component when defining connectivity, etc. If
       the name is not defined, then the component is referenced by the full
       component name.  In this case, 'data_unpack_uc_b'. This becomes
       problematic when two instances of a component are required as with the
       fir_filter_scaled_s' component in this application. -->
    <property name="msb_first" value="true"/>
  </instance>
  <!-- The 'symbol_mapper_b_s' component translates input binary values to a
       signed short integer level.-->
  <instance component="ocpi.comp.sdr.communication.symbol_mapper_b_s"
            name="sym_map" connect="zero_pad">
    <property name="mark_symbol" value="32767"/>
    <!-- Binary '1' = mark_symbol -->
    <property name="space_symbol" value="-32767"/>
    <!-- Binary '0' = space_symbol -->
  </instance>
  <!-- The 'zero_padder_s' component is intended to increase the number of
       samples per symbol, essentially upsampling in preparation for
       filtering.-->
  <instance component="ocpi.comp.sdr.dsp.zero_padder_s" name="zero_pad"
             connect="tx_filter">
    <property name="samples" value="9"/>
    <!-- Adds 9 zeros generating 10 Samples Per Symbol -->
  </instance>
  <!-- The 'fir_filter_scaled_s' applies tap coeffiecents and scales the output
       samples. For this application, the component is defined as havimg 64
       taps and implement a Root raised cosine (RRC) filter to condition the
       input symbols for FSK modulation and mitigate intersymbol
       interference (ISI).

       The RRC is implemented with Beta=0.4, 10 SPS, and a span of 6 with a
       derived filter length of 61 samples. Last 3 taps of the 64 tap component
       filter are set to 0. -->
  <instance component="ocpi.comp.sdr.dsp.fir_filter_scaled_s" name="tx_filter"
            connect="tx_gain">
    <property name="taps" valuefile="./idata/rrc_b0.4_sps10_span6_fir.props"/>
    <property name="number_of_taps" value="64"/>
    <property name="scale_factor" value="13"/>
    <!--  DC gain of filter is sum of taps = 32767  -->
    <!--  Zero padded by x10, so actual DC gain will be 3276.7  -->
    <!--  ceil(log2(3276.7)) = ceil(11.67) = 12  -->
    <!--  Max value will be larger than this, so will need to leave some head
          room.  -->
    <!--  Round up by 1 to a scale factor of 13  -->
    <!--  This gives a Filter DC gain of 3276.7 / 2^13 = 0.39999  -->
    <!-- This property is associated with the HDL worker implementation of
         'fir_filter_scaled_s' component and not RCC.
         To use when HDL model specified the following option must be declared
          with ocpirun:
                   -p tx_filter=number_of_multipliers=8
                   <property name="number_of_multipliers" value="8"/> -->
  </instance>
  <!-- The 'gain_s_l' scales the input to the frequency modulator defining
       frequency deviation.-->
  <instance component="ocpi.comp.sdr.math.gain_s_l" name="tx_gain"
            connect="fm_mod">
    <property name="gain" value="32767"/>
  </instance>
  <!-- The input amplitude of the 'frequency_modulator_l_xs' component
       determines the scale of output frequency deviation. A positive amplitude
       generates a positive frequency and a negative amplitude generates a
       negative frequency.-->
  <instance component="ocpi.comp.sdr.dsp.frequency_modulator_l_xs" name="fm_mod"
            connect="drc" from="output" to="tx" index="0">
    <property name="modulator_amplitude" value="19872"/>
  <!-- The output port of this component connects to the Digital Radio
       Controller's (DRC) 'tx' port and terminates the FSK application's
       transmit path.  The components of the DRC process the remaining TX path
       functionality such as resampling, gain, DAC, tuning, etc. for a specifeid
       SDR platform.-->
  </instance>
  <!--
  ***********************************
  Receive FSK path
  ***********************************
  -->
  <!-- Data samples from the platform's RF frontend are passed from the Digital
       Radio Controller's (DRC) 'rx' port to the Application's receive FSK path.
  -->
  <connection>
    <port instance="drc" name="rx" index="0"/>
    <port instance="mla" name="input"/>
  </connection>

  <!-- The message length adapter is required to regulate message size
       disparities when intermixing signal data and other opcode 
       messages. --> 
  <instance component="ocpi.comp.sdr.interface.message_length_adjuster"
            name="mla" connect='fm_demod'>
        <property name='width' value='32'/>
        <property name="message_size" value="4096"/>
        <property name="timeout" value="0"/>
  </instance>
  <!-- The 'frequency_demodulator_xs_s' initates the FSK application receiver
       path. The output of this component is the measured angle between received
       samples.-->
  <instance component="ocpi.comp.sdr.dsp.frequency_demodulator_xs_s"
            name="fm_demod" connect="rx_filter"/>
  <!-- The receiver filter is matched with the TX RRC filter. It smooths out the
       angular values produced by 'fm_demod' where positive angular rotation
       produce positive amplitudes and negative angular rotation produces
       negative amplitudes.-->
  <instance component="ocpi.comp.sdr.dsp.fir_filter_scaled_s" name="rx_filter"
            connect="edge_detect">
    <property name="taps" valuefile="idata/rrc_b0.4_sps10_span6_fir.props"/>
    <property name="number_of_taps" value="64"/>
    <property name="scale_factor" value="17"/>
    <!-- This property is associated with the HDL worker implementation of
         'fir_filter_scaled_s' component and not RCC. To use when HDL model
          specified the following option must be declared with ocpirun:
                     -p rx_filter=number_of_multipliers=8
                     <property name="number_of_multipliers" value="8"/> -->
  </instance>
  <!-- The 'threshold_s_b' component detects the crossing of a specified
       threshold of a short integer input data sequence and produces a binary
       output representitive of the crossing point.-->
  <instance component="ocpi.comp.sdr.dsp.threshold_s_b" name="edge_detect"
            connect="mov_avg">
    <property name="threshold" value="0"/>
    <property name="invert_output" value="false"/>
  </instance>
  <!-- The 'moving_average_filter_b' is used to further smooth out the received
       symbols by averaging over a symbol period of 10 samples. It generates a
       value of 1 if more samples of the 10 are positive and 0 if number of
       negtive value are greater or equal to negtive value.-->
  <instance component="ocpi.comp.sdr.dsp.moving_average_filter_b" name="mov_avg"
            connect="clock_sync">
    <!-- Averages over the symbol period of 10 samples
         The moving_average_filter_b component worker for HDL and RCC are
         implemented different properties to specify length.

         RCC Property:
                <property name="moving_average_size" value="10"/>
                use option ( -p mov_avg=moving_average_size=10 )

         HDL Property:
                <property name="maximum_moving_average_size" value="10"/>
                use option ( -p mov_avg=maximum_moving_average_size=10 ) -->
  </instance>
  <!-- This application does not directly implement precise symbol clock
       recovery. Bit recovery is accomplished by selecting a sample within the
       synbol period that accounts for symbol overlaps mitigating intersymbol
       interference.-->
  <instance component="ocpi.comp.sdr.communication.clock_sync_edge_detector_b"
            name="clock_sync" connect="detect_split">
    <property name="samples_per_symbol" value="10"/>
    <property name="sample_point" value="4"/>
  </instance>
  <!-- The 'splitter_b' is used to direct the data samples along two paths for
       detection of the HEADER and TAIL pattern bytes added to the data
       sequence by component 'build_packet_us' during tranmit path processing.
  -->
  <instance component="ocpi.comp.sdr.interface.splitter_b" name="detect_split"/>
  <!-- As an alternative to using the 'connect' option during component
       instantiation, the <connection> tag may be used to specify connectivity
       between two component ports. The convention is that the output 'from'
       port is specified first and then the input 'to' port of the next
       component is specified second. -->
  <connection>
    <port instance="detect_split" name="output_1"/>
    <port instance="start_pattern" name="input"/>
  </connection>
  <connection>
    <port instance="detect_split" name="output_2"/>
    <port instance="stop_pattern" name="input"/>
  </connection>
  <!-- The Application defines two instances of the 'pattern_detector_b'
       component. One detects the HEADER bytes and the other TAIL bytes. -->
  <instance component="ocpi.comp.sdr.communication.pattern_detector_b"
            name="start_pattern" connect="gate" from="output" to="input_open">
    <property name="pattern" value="0x55555555faceface"/>
    <!-- HEADER Pattern -->
    <property name="bitmask" value="0x7fffffffffffffff"/>
  </instance>
  <instance component="ocpi.comp.sdr.communication.pattern_detector_b"
            name="stop_pattern" connect="gate" from="output"
            to="input_close">
    <property name="pattern" value="0x5efacefa55555555"/>
    <!-- TAIL Pattern -->
    <property name="bitmask" value="0x7fffffffffffffff"/>
  </instance>
  <!-- The 'discontinuty_gate_b' component detects the reception of
       discontinuity opcodes. The classification to 'open' or 'close' the
       output port depends on which input port of the 'gate' the discontinuity
       opcode is received. In the FSK application, the gate remains closed
       until the HEADER is detected.  Once the HEADER is detected, the PAYLOAD
       data is directed to the 'gate' output port. Once the TAIL pattern is
       detected, the component 'stop_pattern' sends a discontinuity opcode to
       the 'input_close' port of 'gate'.
       This indicated the end of PACKET data and the output port of 'gate' no
       longer sends data. This approach in unconventional but used to
       facilitate the application as an example for instruction.-->
  <instance component="ocpi.examples.discontinuity_gate_b" name="gate"
            connect="pack_bits"/>
  <!-- The 'data_pack_b_uc' component rebuilds the unsigned character data
       bytes from the received bit data by converting 8 bit data bits to
       single data byte.-->
  <instance component="ocpi.comp.sdr.communication.data_pack_b_uc"
            name="pack_bits" connect="file_write">
    <property name="ocpi_debug" value="false"/>
    <property name="ocpi_endian" value="little"/>
  </instance>
  <!--
  ***********************************
  Received Data Sink
  ***********************************
  -->
  <!-- The 'file_write' component completes the receive FSK path and is the
       last component of the FSK application. This component opens the
       specified filename to write binary data. Input data bytes are written
       to the file until the application signals that it is 'done'.  This
       occurs when the 'gate' component detects the last PAYLOAD byte.-->
  <instance component="ocpi.core.file_write" container="rcc0">
    <property name="filename" value="odata/opencpi.jpg"/>
    <property name="messagesinfile" value="false"/>
  </instance>
  <!--
  ***********************************
  Digital Radio Controller (DRC)
  ***********************************
  -->
  <!-- The DRC provides the connectivty betwieen the application's transmit and
       receive processing paths.  The DRC processes the components that are
       required for the application to interact with the transmit and receive RF
       elements such as the DAC, ADC, LO tuners, physical bandwidth filters,
       power amplifier gain, low noise amplifier gain, etc. The DRC is unique to
       each platform and specified configuration. The DRC is typically comprised
       of OpenCPI CIC Interpolator, CIC Decimator, and Complex Mixer components.

       The DRC worker is typically implemented for the specified platform and
       found in the platform's project directory.  Currently there is a
       drc_csts.rcc worker for each supported platform to allow
       selection of txrx, tx, and rx configuration based on the properties
       specified below. The HDL platform DRC must be built to execute on the
       operating host RCC platform (e.g. ubuntu18_04, xilinx19_2_aarch32, etc.)

       The DRC workers for each supported HDL platform are foundas follows:
           [E310]
                ~/opencpi/projects/osp/ocpi.osp.e31x/hdl/cards/drc_csts.rcc/
           [PlutoSDR]
                ~/opencpi/projects/osp/ocpi.osp.plutosdr/hdl/
                  devices.drc_csts.rcc/
           [Zed]
                ~/opencpi/projects/assets/hdl/cards/drc_csts.rcc/
  -->
  <instance component="ocpi.platform.drc">
    <property name="configurations"
              value="{description first,channels {
            {   rx true,
                tuning_freq_mhz 2450.0,
                offset_freq_mhz 0.0,
                bandwidth_3db_mhz 0.5,
                sampling_rate_msps 0.5,
                samples_are_complex true,
                gain_mode auto,
                tolerance_bandwidth_3db_mhz 0.01,
                tolerance_tuning_freq_mhz 0.01,
                tolerance_sampling_rate_msps 0.01,
                tolerance_gain_db 1},
           {    rx false,
                tuning_freq_mhz 2450.0,
                offset_freq_mhz 0.0,
                bandwidth_3db_mhz 0.5,
                sampling_rate_msps 0.5,
                samples_are_complex true,
                gain_mode manual,
                gain_db 0,
                tolerance_bandwidth_3db_mhz 0.01,
                tolerance_tuning_freq_mhz 0.01,
                tolerance_sampling_rate_msps 0.01,
                tolerance_gain_db 1}
          }
        }"/>
    <property name="start" value="0"/>
    <property name="tx_disable_on_flush" value="false"/>
    <property name="enable_rx" value="true"/>
    <property name="enable_tx" value="true"/>
  </instance>
</application>
