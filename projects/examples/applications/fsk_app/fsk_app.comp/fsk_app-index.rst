.. fsk_app_txrx documentation


.. _fsk_app-application:


FSK APPLICATION (``fsk_app_txrx``)
==================================
This application is intended to facilitate a new user’s understanding of the OpenCPI framework structure, show ease of usage, and convey value of portability.   An OpenCPI application typically defines a specified signal processing functionality implemented using OpenCPI project components. 

Description
-----------
This application sends the contents of a data file (e.g., idata/opencpi.jpg) using a standard frequency shift keying (FSK) modulation scheme between the transmit and receiver ports of an OpenCPI SDR platform.

There are are three versions of this application.  

``fsk_app_txrx.xml`` (Application that executes platform rf loopback)

``fsk_app_tx.xml``   (Application that executes platform transmit only)

``fsk_app_rx.xml``   (Application that executes platform receive only)

The first figure shows the relationship between the required OpenCPI files that define the interconnectivity of the Application with the SDR platform. 

Diagram showing OpenCPI Application structure and interaction relationship
--------------------------------------------------------------------------

.. figure:: svg/01_app_struct.svg

Files for Application execution
-------------------------------

.. figure:: svg/02_app_struct_file_list.svg

Platform configuration workers
------------------------------

.. figure:: svg/04_platform_workers.svg

.. figure:: svg/05_platform_workers_list.svg

Worker color code designating worker types in the diagrams and tables
---------------------------------------------------------------------
.. figure:: svg/03_worker_color_code.svg

Digital Radio Controller (DRC) workers
--------------------------------------

.. figure:: svg/06_configuration_workers.svg

.. figure:: svg/07_configuration_workers_list.svg

HDL Assembly workers
--------------------

.. figure:: svg/09_hdl_assembly_workers_HDL.svg

.. figure:: svg/10_hdl_assembly_workers_RCC.svg

HDL Containers
--------------

.. figure:: svg/13_container_hdl_assy.svg

.. figure:: svg/14_container_hdl_assy_drc_only.svg

FSK Application workers
-----------------------

.. figure:: svg/12_fsk_application.svg

.. figure:: svg/15_fsk_app_component_list.svg

Hardware Portability
--------------------
This application has been verified to execute on platforms:

        Ettus E310
        Analog Devices Pluto-ADALM
        Divergent Zedboard

Execution
---------

Prerequisites
~~~~~~~~~~~~~
Skeleton outline: A list of steps that need to be taken before the application can be executed.

Command(s)
~~~~~~~~~~
Skeleton outline: A list of commands to run in order to execute the application.

Verification
------------
Skeleton outline: Possible outputs of the application.

Troubleshooting
---------------
Skeleton outline: List any known issues and and potential fixes here.

**If the above template is not appropriate, consider replacing sections "Execution", "Verification" and "Troubleshooting" with the following:**

Building the App
----------------
Skeleton outline: Include dependencies and any prerequisite steps that need to be taken to build the application.

Testing the App
---------------
Skeleton outline: Information on sample test setups, required artifacts, executable arguments and expected results. Also include any known issues here, if any.

Worker Properties
-----------------
Skeleton outline: A list of the configurable properties for each of the workers in the application.

Worker Artifacts
----------------
Skeleton outline: Any artifacts that are produced by the application.
