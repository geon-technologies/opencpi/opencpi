.. discontinuity_gate_b.rcc RCC worker


.. _discontinuity_gate_b.rcc-RCC-worker:


``discontinuity_gate_b.rcc`` RCC Worker
=======================================
Skeleton outline: Optional summary of the implementation of this worker. Anything before the next heading will be included as worker summary on component documentation page.

Detail
------
.. ocpi_documentation_worker::

.. Skeleton comment: If not a HDL worker / implementation then the below
   section and directive should be deleted. This comment should be removed in
   the final version of this page.

Utilization
-----------
.. ocpi_documentation_utilization::
