//*****************************************************************************
// build_packet_uc-rcc
//
// This worker supports the OpenCPI FSK demonstration application. It receives
// samples from the file_read.rcc worker which currently does not support the
// timed-sample interface protocol. When an OpenCPI user runs the application,
// this worker first outputs a sequence of HEADER bytes followed by the SAMPLE
// bytes received from the file_read worker.  This worker continues to output
// sample bytes until it receives the end-of-file (EOF) signal from the
// file_read worker. Once the worker receives the EOF signal, it sends a
// sequence of TAIL bytes to indicate end of packet.    
//
//	Interfaces:
//		input: uint8_t port
//	       output: uchar_time_sample-prot 
//
// The 'bypass_done' property allows component to be reset to continue
// processing new input data once the tail pattern has been sent for previous
// data.  If set to 'false', then component only processes one set of data
// input and sets application state to 'RCC_DONE'. This feature supports a one
// -shot transmission of data without need to set the time argument for
// running the example FSK application.  [ NOT FULLY IMPLEMENTED YET]   
//
// ****************************************************************************

#include "build_packet_uc-worker.hh"
#include "include/packet_sync_bytes.h"
#include <vector>
#include <unistd.h>

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Build_packet_ucWorkerTypes;

class Build_packet_ucWorker : public Build_packet_ucWorkerBase {

    /* Controls sequence of packet build.  Only one set true at a time */
    bool add_header = true;
    bool add_payload = false;
    bool add_tail = false;
    bool done = false;
    bool bypass_done = false;
    int count = 0;

  RCCResult run(bool /*timedout*/) {
       auto &port = this->input;

      /************************************************************************
       * 'done' set when send tail in complete and property 'bypass_done' =
       * 'false'. If 'done' then send discontinuity opcode.  The discontinuity
       * is sent through processing path component to indicate the tail bytes
       * are complete.    
       ************************************************************************/
       if (done){
           count++;
           if(count == 2000000){
              // output.setOpCode(Uchar_timed_sampleDiscontinuity_OPERATION);
               output.advance();
               return RCC_DONE;
           }
       }

      /************************************************************************
       *  ADD HEADER BYTES
       *
       *  First send HEADER bytes to the output port. This is the start of the
       *  packet. The header bytes are uses to perform intial RX synchronization
       *  and for RX to identify the start of the received packet.
       ************************************************************************/
       if (add_header){
           uint8_t* outData = output.sample().data().data();
           int output_length = sizeof(PACKET_HEADER);
           output.sample().data().resize(output_length);
           for (int i = 0; i < output_length; i++){
               outData[i] = PACKET_HEADER[i];
           }  
           add_header = false;
           add_payload = true;
           output.advance();
       }

       /************************************************************************
        *  ADD PAYLOAD BYTES
        *
        *  Payload bytes are recevied on the input 'port'. For the FSK example
        *  app, the payload is sent by the file_read component. The payload app
        *  is complete when an end-of-file (EOF) is received on the port. 
        ***********************************************************************/
       if (add_payload){

           if (port.eof()) {
               printf("build_packet_uc : EOF received\n");
               add_payload = false;
               add_tail = true;
		
               return RCC_ADVANCE;
           }	

           if(port.hasBuffer() && output.hasBuffer()) {	
               uint8_t *inputData = port.data();
               int input_length = port.length();
               uint8_t* outputData = output.sample().data().data();
               output.sample().data().resize(input_length);
               for (int i = 0; i < input_length; i++){
                   outputData[i] = *inputData;
                   inputData++;
               }
               outputData = inputData;
               return RCC_ADVANCE;
           }
       }

      /*************************************************************************
       *  ADD TAIL BYTES
       *
       *  When payload is complete following receiving an EOF signal, add TAIL
       *  bytes to end of packet.     
       ************************************************************************/
       if (add_tail){

           uint8_t* outData = output.sample().data().data();
           int output_length = sizeof(PACKET_TAIL);
           output.sample().data().resize(output_length);

           for (int i = 0; i < output_length; i++){
               outData[i] = PACKET_TAIL[i];
               }

           add_tail = false;
           bypass_done = properties().bypass_done;
		
           if (bypass_done) {
             // [ NOT IMLEMENTED YET ]
             // Reset to receive new data packet
             //add_header = true;
           }
           else{
	       // Sets application state to RCC_DONE
               done = true;
           }
           output.advance();
           }

           if (!add_header && !add_payload && !add_tail)
               return RCC_OK;

           return RCC_OK;
  }
};

BUILD_PACKET_UC_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
// YOU MUST LEAVE THE *START_INFO and *END_INFO macros here and uncommented
// in any case
BUILD_PACKET_UC_END_INFO
