.. build_packet_uc documentation

.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive.

.. meta::
   :keywords: skeleton example


.. _build_packet_uc:


SKELETON NAME (``build_packet_uc``)
===================================
Skeleton outline: Single line description.

Function
--------
Skeleton outline: The functionality of the component: how it should produce outputs and volatile property values based on inputs and parameter/initial/writable property values (not **how** it is implemented, as that belongs in worker documentation).

The mathematical representation of the component function is given in :eq:`build_packet_uc-equation`.

.. math::
   :label: build_packet_uc-equation

   y[n] = \alpha * x[n]


In :eq:`build_packet_uc-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * Skeleton, etc.,

A block diagram representation of the component function is given in :numref:`build_packet_uc-diagram`.

.. _build_packet_uc-diagram:

.. figure:: build_packet_uc.svg
   :alt: Skeleton alternative text.
   :align: center

   Caption text.

Interface
---------
.. literalinclude:: ../specs/build_packet_uc-spec.xml
   :language: xml

Opcode Handling
~~~~~~~~~~~~~~~
Skeleton outline: Description of how the non-stream opcodes are handled.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   property_name: Skeleton outline: List any additional text for properties, which will be included in addition to the description field in the component specification XML.

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../build_packet_uc.hdl ../build_packet_uc.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * Skeleton outline: List primitives or other files within OpenCPI that are used (no need to list protocols).

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * Skeleton outline: Any other standard C++ or HDL packages.

Limitations
-----------
Limitations of ``build_packet_uc`` are:

 * Skeleton outline: List any limitations, or state "None." if there are none.

Testing
-------
.. ocpi_documentation_test_platforms::

.. Removed ocpi_documentation_test_result_summary directive until it is functional
