OCPISERVE(1)
===========


NAME
----
ocpiserve - command-line tool for making local OpenCPI containers available
for remote access


SYNOPSIS
--------
*`ocpiserve`* ['<options>']


DESCRIPTION
-----------
When an application is run, the OpenCPI framework finds "local"
containers - those that are part of or directly attached to
the local system - where workers based on artifacts can execute.
A local system can use the *`ocpiserve(1)`* tool to offer its
local containers to other network-based systems for use as
"remote" containers.

The *`ocpiserve`* command-line tool is part of OpenCPI's
*'remote containers'* feature, which adds containers
available in other systems on the network to the set
of containers considered for application execution.

The *`ocpiserve`* tool runs as a 'container server', serving up
local containers as remotely accessible containers to network clients.
Remote clients use *`ocpirun`* options to find the systems on the
network that are running *`ocpiserve`* and list the available containers.

When started, *`ocpiserve`* discovers all local network interfaces and
prepares to be contacted by clients for all of them. It optionally (if the
*`--discoverable`* option is used) also
prepares to receive multicast queries from clients on all network
interfaces. The *`ocpiserve`* container server is a fully multi-homed
server, using all network interfaces for normal use (TCP) discovery (via the
User Datagram Protocol (UDP)).

The  *`ocpiserve`* container server does not run, initiate or control
entire OpenCPI applications, so it requires fewer resources than
*`ocpirun`* or ACI executables.

'Artifacts' - binary executables compiled from OpenCPI 'workers' -
are not required to reside on the system running
*`ocpiserve`*. Instead, they are downloaded on demand
to the *`ocpiserve`* container server from
the remote system on which the application is initiated
and controlled and then cached for possible re-use. The *`ocpiserve`*
container server maintains this cache in a directory
named *`artifacts`* unless the *`--directory`* option is used
to change it. The cache is normally maintained after *`ocpiserve`*
exits unless the *`--remove`* option is used to delete it.

Starting *`ocpiserve`* locally requires logging in to the console on
the system with a username and password. CTRL/C can be used to
exit *`ocpiserve`* locally. Alternatively,
the link:ocpiremote.1.html[ocpiremote(1)] tool can be used to start,
stop, and otherwise control an *`ocpiserve`* container server
from a remote client.


OPTIONS
-------
In addition to the general-purpose options common to all OpenCPI
tools (see link:opencpi.1.html[opencpi(1)]),
the options described below can be specified for *`ocpiserve`* operations.
For convenience, options can occur anywhere in the command.
When invoked without options, *`ocpiserve`* displays help information.

In the following descriptions, a plus sign (+) after the option indicates
that it can be specified more than once on the command line.
Arguments to word-form options are preceded by an equal sign (=)
(as shown in the following descriptions) or a space. Arguments to
letter options are preceded by a space.

*`-D`* '<directory-name>', *`--directory=`* '<directory-name>'::
    Specify the name of a directory to use for caching downloaded
    artifacts. The default directory name is *`artifacts`*.

*`-L`*, *`--loopback`*::
    Enable/add UDP multicast discovery on the local loopback subnet.

*`-O`*, *`--onlyloopback`*::
    Allow UDP multicast discovery 'only' on the local loopback subnet.

*`-a`* '<file>', *`--addresses=`*'<file>'::
    Specify a file to which *`ocpiserve`* should write all its TCP/IP
    addresses. See the description of the OCPI_SERVER_ADDRESSES_FILE
    in the 'OpenCPI Application Development Guide' or *`ocpiremote`* man page for details.

*`-d`*, *`--discoverable`*::
    Enable *`ocpiserve`* to receive UDP multicast discovery
    queries from clients on all network interfaces.

*`-n`* '<number-of-RCC-containers>', *`--processors=`*'<number-of-RCC-containers>'::
    Specify the number of RCC containers to create and serve. The default is 1.

*`-p`* '<value>', *`--port=`* '<value>'::
    Specify the TCP port number to use for *`ocpiserve`* ports.
    The default is to use dynamically assigned port numbers.

*`-r`*, *`--remove`*::
    Remove the artifact cache when *`ocpiserve`* exits.

EXAMPLES
---------
. Start the *`ocpiserve`* container server, allowing it
to be discoverable via UDP multicast from client systems
and displaying status and progress information:
+
---------------------------------
ocpiserve --verbose --discoverable
---------------------------------
+
. Start the *`ocpiserve`* container server and direct it
to write its TCP/IP addresses to the file named *`myserveraddrs`*:
+
---------------------------------
ocpiserve --address=myserveraddrs
---------------------------------
+
. Start the *`ocpiserve`* container server and direct
it to use the TCP port 9233 as the TCP port:
+
---------------------
ocpiserve --port=9233
---------------------

BUGS
----
See https://www.opencpi.org/report-defects

RESOURCES
---------
See the main web site: https://www.opencpi.org

See the 'OpenCPI Application Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Application_Development_Guide.pdf

See the 'OpenCPI User Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_User_Guide.pdf

SEE ALSO
--------
link:ocpidev.1.html[ocpidev(1)]
link:ocpidev-application.1.html[ocpidev-application(1)]
link:ocpidev-assembly.1.html[ocpidev-assembly(1)]
link:ocpidev-worker.1.html[ocpidev-worker(1)]
link:ocpiremote.1.html[ocpiremote(1)]
link:ocpirun.1.html[ocpirun(1)]

COPYING
-------
Copyright \(C) 2023 OpenCPI www.opencpi.org. OpenCPI is free software:
you can redistribute it and/or modify it under the terms of the
GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your
option) any later version.

